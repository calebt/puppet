# == Define: apache::vhost
#
# Define an Apache virtual host resource.
#
# === Parameters
#
# *servername*:    (namevar) The host name that the virtual host uses to
#                  identify itself.
# *ports*:         An array of ports to match. You must ensure that Apache is
#                  listening on all ports listed. Defaults to 80.
# *docroot*:       An absolute path to the directory tree visible from the web.
#                  Required unless 'source' is defined.
# *serveraliases*: An array of alternate names for the virtual host.
# *priority*:      Set the priority of the virtual host. Defaults to 25.
# *template*:      The virtual host template file to use. Defaults to
#                  templates/vhost-default.erb
# *options*:       A list of options available in the docroot directory.
#                  See http://httpd.apache.org/docs/2.2/mod/core.html#options
#                  for a list of available options.
# *vhost_name*     The ip address for name-based virtual hosts. Must have a
#                  matching NameVirtualHost. Defaults to '*'.
# *source*:        For advance use only. The path to a souce file that declares
#                  the virtual host directly.
#
# === Examples
#
# apache::vhost {'site.example.com':
#   ports         => [80, 8888],
#   docroot       => '/var/www/site.example.com',
#   serveraliases => ['site1.example.com', 'site2.example.com'],
#   vhost_name    => 'localhost',
# }
#
# apache::vhost {'default':
#   source   => 'puppet:///modules/apache/vhost-default',
#   priority => '000',
# }
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

define apache::vhost(
    $servername    = $name,
    $ports         = 80,
    $docroot       = '',
    $serveraliases = '',
    $priority      = '25',
    $template      = 'apache/vhost-default.erb',
    $options       = "Indexes FollowSymLinks MultiViews",
    $vhost_name    = '*',
    $source        = '',
  ){

  if $source != '' {
    file {"/etc/apache2/sites-available/${name}":
      source => $source,
      owner  => root,
      group  => root,
      mode   => 644,
    }
  }
  else {
    if $docroot != '' {
      file {"{$name} ${docroot}":
        path => $docroot,
        ensure => directory,
        owner  => vagrant,
        group  => www-data,
        mode   => 755,
      }
    }
    else {
      fail("apache::vhost - Docroot is required unless source is defined.")
    }

    file {"/etc/apache2/sites-available/${name}":
      content => template($template),
      owner   => root,
      group   => root,
      mode    => 644,
    }
  }

  file {"/etc/apache2/sites-enabled/${priority}-${name}":
    ensure => link,
    target => "/etc/apache2/sites-available/${name}",
    notify => Service['apache2'],
  }
}
