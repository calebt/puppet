# == Class: apache
#
# Install and configure the Apache webserver.
#
# === Example
#
# include apache
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

class apache {
  include apt

  package {'apache2':
    ensure => latest,
  }

  service {'apache2':
    ensure  => running,
    require => Package['apache2'],
  }

  file {'/etc/apache2/ports.conf':
    ensure  => present,
    source  => 'puppet:///modules/apache/ports.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '644',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  file {'/etc/apache2/apache2.conf':
    ensure  => present,
    source  => 'puppet:///modules/apache/apache2.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '644',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  # Add the default virtual host entry.
  apache::vhost {'default':
    source   => 'puppet:///modules/apache/vhost-default',
    priority => '000',
  }

  Package['apache2'] -> Apache::Vhost <| |>
}
