# == Define: mysql::db
#
# Manage a mysql database.
#
# === Parameters
#
# [*db_name*]
#   (namevar) The database name.
#
# [*user*]
#   The database user. Required.
#
# [*password*]
#   The datbase password. Required.
#
# === Examples
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

define mysql::db::create(
    $db = $name,
    $user,
    $password,
  ){

  exec {"${db}-create":
    unless  => "/usr/bin/mysql -u${user} -p${password} ${db}",
    command => "/usr/bin/mysql -uroot -pvagrant -e \"CREATE DATABASE ${db}; GRANT ALL ON ${db}.* TO ${user}@localhost IDENTIFIED BY '$password';\"",
    require => Service["mysql"],
  }
}

define mysql::db::user (
    $user = $name,
    $password,
  ){

  exec {"${user}-user":
    unless => "/usr/bin/mysql -u${user} -p${password}",
    command => "/usr/bin/mysql -uroot -pvagrant -e \"CREATE USER ${user}@localhost IDENTIFIED BY '$password';\"",
    require => Service["mysql"],
  }
}
